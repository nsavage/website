#+TITLE: Nick Savage
#+SETUPFILE: ../template.org
#+OPTIONS: \n:t

/Et in Arcadia ego/
\\
Hello, I am Nick Savage and this is my personal website. I'm a CPA living in Ottawa, Ontario. I studied history at the Unviersity of Ottawa and went into accounting afterwards. I work at [[https://mccayduff.com/][McCay Duff]], a public accounting firm in Ottawa. I also am a amateur programmer. I've contributed code to various open source projects like [[https://www.gnu.org/software/emacs/][emacs]] and [[https://code.orgmode.org/bzg/org-mode][org-mode]]. I'm a big fan of the Toronto Blue Jays and spending my weekends wandering around the local forests with my son.
\\

I am also available for any consulting or advisory projects. I know that the accounting side of startups is challenging and not usually a high priority in the early days. I have expertise in accounting, bookkeeping, reporting and other business functions like that. I'm also familiar with python, SQL, and have an interest and experience in automating business processes. I could also be a good resource if you have any particular financial reporting or tax questions that may require research.

* Contact

**** [[mailto:nick@nicksavage.ca][Email]] [[file:static/mail.png]]
**** [[https://gitlab.com/nsavage][Gitlab]] [[file:./static/gitlab.png]]
**** [[https://github.com/NickSavage][Github]] [[file:./static/github.png]]
**** [[https://www.linkedin.com/in/nicholasbsavage/][LinkedIn]] [[file:./static/linkedin.png]]
